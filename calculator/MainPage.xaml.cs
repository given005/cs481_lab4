﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace calculator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        double first;
        double current = 0;
        public MainPage()
        {
            
                Title = "Calculator - C#";
                BackgroundColor = Color.FromHex("#404040");

                var plainButton = new Style(typeof(Button))
                {
                    Setters = {
                new Setter { Property = Button.BackgroundColorProperty, Value = Color.FromHex ("#eee") },
                new Setter { Property = Button.TextColorProperty, Value = Color.Black },
                new Setter { Property = Button.BorderRadiusProperty, Value = 0 },
                new Setter { Property = Button.FontSizeProperty, Value = 40 }
        }
                };
                var darkerButton = new Style(typeof(Button))
                {
                    Setters = {
      new Setter { Property = Button.BackgroundColorProperty, Value = Color.FromHex ("#ddd") },
      new Setter { Property = Button.TextColorProperty, Value = Color.Black },
      new Setter { Property = Button.BorderRadiusProperty, Value = 0 },
      new Setter { Property = Button.FontSizeProperty, Value = 40 }
    }
                };
                var OrangeButton = new Style(typeof(Button))
                {
                    Setters = {
                    new Setter { Property = Button.BackgroundColorProperty, Value = Color.FromHex ("#E8AD00") },
                    new Setter { Property = Button.TextColorProperty, Value = Color.White },
                    new Setter { Property = Button.BorderRadiusProperty, Value = 0 },
                    new Setter { Property = Button.FontSizeProperty, Value = 40 }
    }
                };

                var controlGrid = new Grid { RowSpacing = 1, ColumnSpacing = 1 };
                controlGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(150) });
                controlGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                controlGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                controlGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                controlGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                controlGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

                controlGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                controlGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                controlGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                controlGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                var label = new Label
                {
                    Text = "0",
                    HorizontalTextAlignment = TextAlignment.End,
                    VerticalTextAlignment = TextAlignment.End,
                    TextColor = Color.White,
                    FontSize = 60
                };
                controlGrid.Children.Add(label, 0, 0);

                Grid.SetColumnSpan(label, 4);

                //Sets up the buttons
                controlGrid.Children.Add(new Button { Text = "C", Style = darkerButton }, 0, 1);
                controlGrid.Children.Add(new Button { Text = "div", Style = OrangeButton }, 3, 1);
                controlGrid.Children.Add(new Button { Text = "7", Style = plainButton }, 0, 2);
                controlGrid.Children.Add(new Button { Text = "8", Style = plainButton }, 1, 2);
                controlGrid.Children.Add(new Button { Text = "9", Style = plainButton }, 2, 2);
                controlGrid.Children.Add(new Button { Text = "X", Style = OrangeButton }, 3, 2);
                controlGrid.Children.Add(new Button { Text = "4", Style = plainButton }, 0, 3);
                controlGrid.Children.Add(new Button { Text = "5", Style = plainButton }, 1, 3);
                controlGrid.Children.Add(new Button { Text = "6", Style = plainButton }, 2, 3);
                controlGrid.Children.Add(new Button { Text = "-", Style = OrangeButton }, 3, 3);
                controlGrid.Children.Add(new Button { Text = "1", Style = plainButton }, 0, 4);
                controlGrid.Children.Add(new Button { Text = "2", Style = plainButton }, 1, 4);
                controlGrid.Children.Add(new Button { Text = "3", Style = plainButton }, 2, 4);
                controlGrid.Children.Add(new Button { Text = "+", Style = OrangeButton }, 3, 4);
               
                controlGrid.Children.Add(new Button { Text = "=", Style = OrangeButton }, 3, 5);

                var zeroButton = new Button { Text = "0", Style = plainButton };
                controlGrid.Children.Add(zeroButton, 0, 5);
                Grid.SetColumnSpan(zeroButton, 2);

                Content = controlGrid;

            InitializeComponent();

        }

        void Button_ClickedNum(object sender, EventArgs e)
        {
            //Button Events
            Button button = (Button)sender;
            string num = button.Text;
            this.Space.Text = num;
           
        }
        //You can do basic math here
        void Button_ClickedAdd(object sender, EventArgs e)
        {
            double num;
            num = Double.Parse(this.Space.Text);
            first = num;
            current = 1;
        }
        void Button_ClickedSub(object sender, EventArgs e)
        {
            double num;
            num = Double.Parse(this.Space.Text);
            first = num;
            current = 2;
        }
        void Button_ClickedMul(object sender, EventArgs e)
        {
            double num;
            num = Double.Parse(this.Space.Text);
            first = num;
            current = 3;
        }

        void Button_ClickedDiv(object sender, EventArgs e)
        {
            double num;
            num = Double.Parse(this.Space.Text);
            first = num;
            current = 4;
        }
       
        //Equals keeps track of how many digits
        void Button_ClickedEq(object sender, EventArgs e)
        {
            if(current == 1)
            {
                double num;
                num = Double.Parse(this.Space.Text);
                num = first + num;
                this.Space.Text = num.ToString();
                current = 0;
            }
            if(current == 2)
            {
                double num;
                num = Double.Parse(this.Space.Text);
                num = first - num;
                this.Space.Text = num.ToString();
                current = 0;       
            }
            if (current == 3)
            {
                double num;
                num = Double.Parse(this.Space.Text);
                num = first * num;
                this.Space.Text = num.ToString();
                current = 0;
            }
            if (current == 4)
            {
                double num;
                num = Double.Parse(this.Space.Text);
                num = first / num;
                this.Space.Text = num.ToString();
                current = 0;
            }
        }

    }
}
